import * as React from "react"
import { HeadFC, PageProps } from "gatsby"
import Layout from "../components/layout"



const NotFoundPage: React.FC<PageProps> = () => {
  return (
    <Layout>

    <h1>NOT FOUND</h1>
    <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
  </Layout>
  )
}

export default NotFoundPage

export const Head: HeadFC = () => <title>Not found</title>

