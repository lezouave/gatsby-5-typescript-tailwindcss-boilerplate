import React from "react"
import {graphql, useStaticQuery} from "gatsby"


import Header from "./header"

const Layout = ({children}) => {

    const data = useStaticQuery(graphql`
        query SiteTitleQuery {
            site {
                siteMetadata {
                    title
                }
            }
        }
    `)
    
    return (
        <>
          
            <Header siteTitle={data.site.siteMetadata.title}/>
            <div
                style={{
                    margin: `0 auto`,
                }}
            >
                <main className="relative">{children}</main>
            
                <div className="layout-footer">
                    <div className="container flex flex-col justify-between py-6 sm:flex-row">
                        <p className="text-center font-body text-white md:text-left">
                            © Copyright 2023. All right reserved.
                        </p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Layout
