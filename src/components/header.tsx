
import { Link } from "gatsby"
import React, { useState } from "react"
import Logo from "../images/svg/logo.svg";
import { useScrollPosition } from "../hooks/useScrollPosition";


export default function Header(){


    const [menuIsOpen, setMenuIsOpen] = useState(false);

    const toggleMenu = () => {setMenuIsOpen(!menuIsOpen)}

    const scrollPosition = useScrollPosition()

    return (
        <nav className="">

  <div className="w-full z-50 top-0 py-3 sm:py-5 fixed bg-darkprimary px-4">
    <div className="flex h-16 items-center justify-between">
      <div className="flex items-center">
      <div>
      <Link to="/">
          <Logo className={scrollPosition > 0 ? 'w-20 h-auto' : 'w-28 sm:w-27 h-auto'}/>
      </Link>
  </div>
        <div className="hidden sm:ml-6 sm:block">
          <div className="flex space-x-4">
            <Link to="/#beny" className="rounded-md bg-gray-900 px-3 md:px-1 py-2 text-3xl md:text-xl font-medium text-white hover:text-secondary">???</Link>
            <Link to="/#mes-services" className="rounded-md px-3 md:px-1 py-2 text-3xl md:text-xl font-medium text-white hover:bg-gray-700 hover:text-secondary">???</Link>
            <Link to="/#portfolio" className="rounded-md px-3 md:px-1 py-2 text-3xl md:text-xl font-medium text-white hover:bg-gray-700 hover:text-secondary">???</Link>
            <Link to="/#clients" className="rounded-md px-3 md:px-1 py-2 text-3xl md:text-xl font-medium text-white hover:bg-gray-700 hover:text-secondary">???</Link>
            <Link to="/#experience" className="rounded-md px-3 md:px-1 py-2 text-3xl md:text-xl font-medium text-white hover:bg-gray-700 hover:text-secondary">???</Link>
            <Link to="/#contact" className="rounded-md px-3 md:px-1 py-2 text-3xl md:text-xl font-medium text-white hover:bg-gray-700 hover:text-secondary">???</Link>
          </div>
        </div>
      </div>
      <div className="-mr-2 flex sm:hidden">
        {/*   Mobile menu button  */}
        <button type="button" onClick={toggleMenu} className="relative z-70 inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white outline-none ring-2 ring-inset ring-white" aria-controls="mobile-menu" aria-expanded="false">
          <span className="absolute -inset-0.5"></span>
          <span className="sr-only">Open main menu</span>
          
           {/*   Icon when menu is closed.  */}

          {/*    Menu open: "hidden", Menu closed: "block"   */}

          {
            !menuIsOpen &&
            <svg className="block h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="white" aria-hidden="true">
            <path strokeLinecap="round" strokeLinejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
          </svg>
        }
         
         
          
          {/*   
            Icon when menu is open.

            Menu open: "block", Menu closed: "hidden"
           */}

           {
            menuIsOpen &&
          <svg className="block h-6 w-6" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="white" aria-hidden="true">
            <path strokeLinecap="round" strokeLinejoin="round" d="M6 18L18 6M6 6l12 12" />
          </svg>
        }

        </button>
      </div>
    </div>
  </div>

  {/*     Mobile menu, show/hide based on menu state.   */}
  {
    menuIsOpen &&
  <div className="sm:hidden w-full bg-softprimary fixed z-40" id="mobile-menu">
    <div className="space-y-1 px-2 pb-3 pt-2">
    {/*     Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white"   */}
    <Link to="/#beny" onClick={toggleMenu} className="block rounded-md bg-gray-900 px-3 py-2 text-base font-medium text-white">???</Link>
    <Link to="/#mes-services" onClick={toggleMenu} className="block rounded-md px-3 py-2 text-base font-medium text-gray-300 hover:bg-gray-700 hover:text-white">???</Link>
    <Link to="/#portfolio" onClick={toggleMenu} className="block rounded-md px-3 py-2 text-base font-medium text-gray-300 hover:bg-gray-700 hover:text-white">???</Link>
    <Link to="/#clients" onClick={toggleMenu} className="block rounded-md px-3 py-2 text-base font-medium text-gray-300 hover:bg-gray-700 hover:text-white">???</Link>
    <Link to="/#experience" onClick={toggleMenu} className="block rounded-md px-3 py-2 text-base font-medium text-gray-300 hover:bg-gray-700 hover:text-white">???</Link>
    <Link to="/#contact" onClick={toggleMenu} className="block rounded-md px-3 py-2 text-base font-medium text-gray-300 hover:bg-gray-700 hover:text-white">???</Link>
    </div>
  </div>
  }
  
</nav>
    )
}