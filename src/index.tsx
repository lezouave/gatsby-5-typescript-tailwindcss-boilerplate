
import React from 'react'
import { graphql, PageProps } from 'gatsby'
import Layout from '../components/layout';
import SEO from "../components/seo"


type DataProps = {
    site: {
        buildTime: string
    }
}



const Index: React.FC<PageProps<DataProps>> = ({ data, path }) => {
    return (

        <Layout>
            <SEO title="Index Page" />
           Index Page
        </Layout>
    )
}


export default Index

export const query = graphql`
    {
        site {
            buildTime(formatString: "YYYY-MM-DD hh:mm a z")
        }
    }
`
